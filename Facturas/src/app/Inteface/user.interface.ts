export interface User{
  result: {
    userName: string,
    name: string,
    surname: string,
    emailAddress: string,
    isActive: boolean,
    fullName: string,
    lastLoginTime: string,
    creationTime: string,
    roleNames: string[],
    id: number
  };
  targetUrl: string;
  success: boolean;
  error: string;
  unAuthorizedRequest: boolean;
  __abp: boolean;
}
