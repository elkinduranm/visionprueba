export interface Iproveedor{
  result: [
    {
      id: string;
      nombre: string;
      identificacion: string;
      tipoIdentificacion: string;
      direccion: string;
      telefono: string;
      regimen: string;
  }];
  targetUrl: string;
  success: boolean;
  error: string;
  unAuthorizedRequest: boolean;
  __abp: boolean;
}
