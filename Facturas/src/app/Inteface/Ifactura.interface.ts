export interface Ifactura{
  result: [
    {
      id: string;
      facturaVenta: string;
      fechaFactura: string;
      idProveedor: string;
      estado: string;
      iva: string;
      total: string;
  }];
  targetUrl: string;
  success: boolean;
  error: string;
  unAuthorizedRequest: boolean;
  __abp: boolean;
}
