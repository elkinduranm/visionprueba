export interface Idetalle{
  result: [
    {
      id: string;
      idFactura: string;
      producto: string;
      cantidad: string;
      valorUnidad: string;
      total: string;
  }];
  targetUrl: string;
  success: boolean;
  error: string;
  unAuthorizedRequest: boolean;
  __abp: boolean;
}
