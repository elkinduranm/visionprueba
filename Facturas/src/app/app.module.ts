import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { app_routing} from './root-routes.module';
import {HttpClientModule} from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import { CookieService } from 'ngx-cookie-service';
import {NgxPaginationModule} from 'ngx-pagination';
import {FormsModule} from '@angular/forms';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FacturaComponent } from './contenido/factura/factura.component';
import { MenuComponent } from './contenido/menu/menu.component';
import { DetalleComponent } from './contenido/detalle/detalle.component';
import { LoginComponent } from './contenido/login/login.component';
import { AprobacionesComponent } from './contenido/aprobaciones/aprobaciones.component';

@NgModule({
  declarations: [
    AppComponent,
    FacturaComponent,
    MenuComponent,
    DetalleComponent,
    LoginComponent,
    AprobacionesComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    NgxPaginationModule,
    app_routing,
    BrowserAnimationsModule
  ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
