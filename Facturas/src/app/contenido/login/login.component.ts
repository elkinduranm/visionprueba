import { MenuService } from './../../servicios/menu.service';
import { ApiserviceService } from './../../servicios/apiservice.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  usuario: string;
  password: string;
  show = false;
  constructor(private router: Router, private authService: ApiserviceService, private menu: MenuService) { }

  ngOnInit(): void {

  }

  login(): void{
    this.show = true;
    this.authService.iniciaSession(this.usuario, this.password, true).subscribe(
      resultado => {
        if (this.authService.getToken() != null){
          this.authService.getPerfil(Number( this.authService.getIdToken())).subscribe(
            respuesta => {
              this.menu.mostrarMenu();
              this.show = false;
              this.router.navigateByUrl('factura');
            }
          );
        }
      }
    );
  }

}
