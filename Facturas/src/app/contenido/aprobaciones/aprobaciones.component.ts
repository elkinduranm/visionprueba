import { element } from 'protractor';
import { MenuService } from './../../servicios/menu.service';
import { ApiserviceService } from './../../servicios/apiservice.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {jsPDF,jsPDFAPI,jsPDFOptions} from 'jspdf';
import 'jspdf-autotable';

@Component({
  selector: 'app-aprobaciones',
  templateUrl: './aprobaciones.component.html',
  styleUrls: ['./aprobaciones.component.css']
})
export class AprobacionesComponent implements OnInit {

  @ViewChild('content') content: ElementRef;
  datos: any;
  head = ['factura', 'fecha de factura', 'proveedor', 'régimen', 'estado de factura', 'IVA', 'TOTAL'];
  p: number = 1;
  factura: string;
  total: number;
  inicial: string;
  final: string;
  detalle: any;
  constructor(private authService: ApiserviceService, private menu: MenuService) { }

  ngOnInit(): void {
    this.menu.mostrarMenu();

  }

  buscar(): void{
    this.authService.getFacturasR(this.inicial).subscribe(resultado => {
      console.log(resultado.result);
      this.datos = resultado.result;
    });
  }

  aprobar(row): void{
    const fd = new FormData();
    fd.append('Id', row.id);
    fd.append('facturaVenta', row.facturaVenta);
    fd.append('fechaFactura', row.fechaFactura);
    fd.append('IdProveedor', row.idProveedor);
    fd.append('estado', 'aprobado');
    fd.append('iva', row.iva);
    fd.append('total', row.total);
    this.authService.RegistrarFactura(fd).subscribe(resultado => {
      const result = resultado['success'];
      if (result){
        alert('Factura Aprobada');
      }
      console.log(resultado['success']);
      this.buscar();
    });
  }
  rechazar(row): void{
    const fd = new FormData();
    fd.append('Id', row.id);
    fd.append('facturaVenta', row.facturaVenta);
    fd.append('fechaFactura', row.fechaFactura);
    fd.append('IdProveedor', row.idProveedor);
    fd.append('estado', 'rechazado');
    fd.append('iva', row.iva);
    fd.append('total', row.total);
    this.authService.RegistrarFactura(fd).subscribe(resultado => {
      const result = resultado['success'];
      if (result){
        alert('Factura rechazado');
      }
      console.log(resultado['success']);
      this.buscar();
    });
  }

  ver(row): void{
    this.factura = row.facturaVenta;
    this.authService.getDetaller(row.id).subscribe(resultado => {

      console.log(resultado);
      this.detalle = resultado.result;
    });
  }
  Reporte(): void{
    const doc = new jsPDF();

    doc.setFontSize(18);
    doc.text('My PDF Table', 11, 8);
    doc.setFontSize(11);
    doc.setTextColor(100);
    let x=8;
    let y=8;


    this.head.forEach(element => {
      doc.rect(x, 20, 30, 10);
      doc.text(element,x,30);
      x= x+30;
    });


    // Open PDF document in new tab
    doc.output('dataurlnewwindow');

    // Download PDF document
    //doc.save('table.pdf');
  }

}
