import { ApiserviceService } from './../../servicios/apiservice.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleComponent implements OnInit {

  detalle: any;
  idDetalle: string;
  cantidad: string;
  descripcion: string;
  valorUnitario: string;
  valorTotal: string;
  @Input() factura: string;
  idFactura: string;
  constructor(private authService: ApiserviceService) { }

  ngOnInit(): void {
    this.idDetalle = '0';
    console.log(this.factura);
    this.buscarFactura();
    this.buscarDEtalles();
  }

  guardarDetalle(): void{
    if (this.factura === '' || this.factura ==  null){
      alert('Debe escribir la información de la factura');
      return;
    }
    const fd = new FormData();

    fd.append('id', this.idDetalle);


    if (this.idFactura !== '' || this.idFactura !== null){
      this.authService.getFactura(this.factura).subscribe(resultado => {
        console.log(resultado.result);
        this.idFactura = resultado.result['id'];
        fd.append('IdFactura', this.idFactura);
        fd.append('producto', this.descripcion);
        fd.append('cantidad', this.cantidad);
        fd.append('valorUnidad', this.valorUnitario);
        fd.append('total', this.valorTotal);
        this.guardar(fd);
      });
    }else{
      fd.append('IdFactura', this.idFactura);
      fd.append('producto', this.descripcion);
      fd.append('cantidad', this.cantidad);
      fd.append('valorUnidad', this.valorUnitario);
      fd.append('total', this.valorTotal);
      this.guardar(fd);
    }

  }

  guardar(datos: any): void{
    this.authService.RegistrarDetalle(datos).subscribe(resultado => {
      const result = resultado['success'];
      if (result){
        alert('Datos registrada');
      }
      console.log(resultado['success']);
      this.buscarDEtalles();
    });
  }

  buscarFactura(): void{

    this.authService.getFactura(this.factura).subscribe(resultado => {
      console.log(resultado.result);
      this.idFactura = resultado.result['id'];
    });
  }

  buscarDEtalles(): void{
    this.authService.getDetaller(this.idFactura).subscribe(resultado => {
      console.log(resultado.result);
      this.detalle = resultado.result;

    });
  }

  editardetalle(row): void{
    this.idDetalle = row.id;
    this.idFactura = row.idFactura;
    this.cantidad = row.cantidad;
    this.descripcion = row.producto;
    this.valorUnitario = row.valorUnidad;
    this.valorTotal = row.total;
  }

  eliminardetalle(row): void{
    this.idDetalle = row.id;
    this.idFactura = row.idFactura;
    this.cantidad = row.cantidad;
    this.descripcion = row.producto;
    this.valorUnitario = row.valorUnidad;
    this.valorTotal = row.total;
    this.eliminar();
  }

  eliminar(): void{
    this.authService.deleteDetalle(this.idDetalle).subscribe(resultado => {
      console.log(resultado.result);
      alert('Detalle eliminado de la factura');
      this.buscarDEtalles();

    });
  }

}
