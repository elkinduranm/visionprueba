import { MenuService } from './../../servicios/menu.service';
import { ApiserviceService } from './../../servicios/apiservice.service';
import { Component, OnInit } from '@angular/core';
import { createThis } from 'typescript';

@Component({
  selector: 'app-factura',
  templateUrl: './factura.component.html',
  styleUrls: ['./factura.component.css']
})
export class FacturaComponent implements OnInit {

  idFactura: string;
  idProveedor: string;
  identificacion: string;
  factura: string;
  tipoDocumento: string;
  nombreProveedor: string;
  regimen: string;
  direccion: string;
  talefono: string;

  fechaFactura: string;
  iva: string;
  total: string;

  proveedores: any;
  constructor(private authService: ApiserviceService, private menu: MenuService) { }

  ngOnInit(): void {
    this.menu.mostrarMenu();
    this.buscarProveedores();

  }

  buscarProveedores(): void{
    this.authService.getProveedores().subscribe(resultado => {
      console.log(resultado);
      this.proveedores = resultado.result;
      console.log(this.proveedores);
    });
  }

  buscarProveedor(): void{

    this.authService.getProveedor(this.identificacion).subscribe(resultado => {
      console.log(resultado.result['id']);
      this.idProveedor = resultado.result['id'];
    });
  }

  guardarProveedor(): void{

    if(this.identificacion === '' || this.identificacion == null){
      alert("Debe escribir datos del proveedor");
      return;
    }
    const fd = new FormData();
    fd.append('id', '0');
    fd.append('nombre', this.nombreProveedor);
    fd.append('identificacion', this.identificacion);
    fd.append('tipoIdentificacion', this.tipoDocumento);
    fd.append('direccion', this.direccion);
    fd.append('telefono', this.talefono);
    fd.append('regimen', this.regimen);
    this.authService.RegistrarProveedor(fd).subscribe(resultado => {
      const result = resultado['success'];
      if (result){
        alert('Datos registrada');
      }
      console.log(resultado['success']);
    });
  }

  guardarFactura(): void{
    if(this.identificacion === '' || this.identificacion == null){
      alert("Debe escribir datos del proveedor");
      return;
    }
    if (this.fechaFactura === '' || this.fechaFactura == null){
      alert("Debe escribir la fecha de la factura");
      return;
    }

    if (this.factura === '' || this.factura == null){
      alert("Debe escribir el número de la factura");
      return;
    }
    this.authService.getProveedor(this.identificacion).subscribe(resultado => {
      console.log(resultado);
      if (resultado.result == null){
        alert("Proveedor no está registrado");
        return;
      }
      this.idProveedor = resultado.result['id'];
      const fd = new FormData();
      fd.append('id', '0');
      fd.append('facturaVenta', this.factura);
      fd.append('fechaFactura', this.fechaFactura.toString());
      fd.append('IdProveedor', this.idProveedor);
      fd.append('estado', 'pendiente');
      fd.append('iva', this.iva);
      fd.append('total', this.total);
      this.authService.RegistrarFactura(fd).subscribe(resultado => {
        const result = resultado['success'];
        if (result){
          alert('Datos registrada');
        }
        console.log(resultado['success']);
      });
    });
    /**/
  }

}
