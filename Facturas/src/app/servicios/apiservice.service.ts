import { Idetalle } from './../Inteface/Idetalle.interface';
import { Ifactura } from './../Inteface/Ifactura.interface';
import { Iproveedor } from './../Inteface/Iproveedor.interface';
import { User } from './../Inteface/user.interface';
import { AuthI } from './../Inteface/authI.interface';
import { CookieService } from 'ngx-cookie-service';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import { Observable } from 'rxjs';
import { tap, timestamp} from 'rxjs/Operators';
import { StringDecoder } from 'string_decoder';

@Injectable({
  providedIn: 'root'
})
export class ApiserviceService {

  private token: string;
  private login = environment.host + 'api/TokenAuth/Authenticate';
  private valida = environment.host + 'api/services/app/Role/GetAll';
  private usuarioAuth = environment.host + 'api/services/app/User/Get';

  private proveedor = environment.host + 'api/Proveedores';
  private factura = environment.host + 'api/Factura';
  private detalle = environment.host + 'api/DetalleFactura';


  constructor(private httpClient: HttpClient, private cookies: CookieService) { }

  /**Detalle factura */
  RegistrarDetalle(datos: FormData): Observable<any>{
    const header = new HttpHeaders()
    .set('Authorization',  `Bearer ${this.getToken()}`);console.log(datos);
    return this.httpClient.post(this.detalle, datos, {headers: header });
  }

  getDetaller(factura: string): Observable<Idetalle>{

    const header = new HttpHeaders()
        .set('Authorization',  `Bearer ${this.getToken()}`);
    return this.httpClient.get<Idetalle>(this.detalle + '/' + factura, {headers: header }   )
    .pipe(
      tap(
        (res: Idetalle) => {
          return res.result;
        }
      )
    );
  }

  deleteDetalle(id: string): Observable<any>{

    const header = new HttpHeaders()
    .set('Authorization',  `Bearer ${this.getToken()}`);
    return this.httpClient.delete(this.detalle + '/' + id, {headers: header});
  }


 /**Factura */
  RegistrarFactura(datos: FormData): Observable<any>{
    const header = new HttpHeaders()
    .set('Authorization',  `Bearer ${this.getToken()}`);
    return this.httpClient.post(this.factura, datos, {headers: header });
  }

  getFactura(identificacion: string): Observable<Ifactura>{

    const header = new HttpHeaders()
        .set('Authorization',  `Bearer ${this.getToken()}`);
    return this.httpClient.get<Ifactura>(this.factura + '/' + identificacion, {headers: header }   )
    .pipe(
      tap(
        (res: Ifactura) => {
          return res.result;
        }
      )
    );
  }

  getFacturasR(inicio: string): Observable<Ifactura>{

    const header = new HttpHeaders()
        .set('Authorization',  `Bearer ${this.getToken()}`);
    return this.httpClient.get<any>(this.factura + '/reporte/' + inicio, {headers: header }   );
  }
  /**proveedores */

  RegistrarProveedor(datos: FormData): Observable<any>{
    const header = new HttpHeaders()
    .set('Authorization',  `Bearer ${this.getToken()}`);
    return this.httpClient.post(this.proveedor, datos, {headers: header });
  }

  getProveedor(identificacion: string): Observable<Iproveedor>{

    const header = new HttpHeaders()
        .set('Authorization',  `Bearer ${this.getToken()}`);
    return this.httpClient.get<Iproveedor>(this.proveedor + '/' + identificacion, {headers: header }   )
    .pipe(
      tap(
        (res: Iproveedor) => {
          return res.result;
        }
      )
    );
  }

  getProveedores(): Observable<Iproveedor>{

      const header = new HttpHeaders()
          .set('Authorization',  `Bearer ${this.getToken()}`);
      return this.httpClient.get<Iproveedor>(this.proveedor,  {headers: header })
        .pipe(
          tap(
            (res: Iproveedor) => {
              return res.result;
            }
          )
        );
  }
  //*** Autenticación */
  iniciaSession(userNameOrEmailAddress: string, password: string, rememberClient: boolean): Observable<AuthI>{

    return this.httpClient.post<AuthI>(this.login, {userNameOrEmailAddress, password, rememberClient})
    .pipe(
      tap(
        (res: AuthI) => {
          // this.saveToken(res.result.accessToken, res.result.expireInSeconds.toString(), res.result.userId.toString());
          this.setToken(res.result.accessToken, res.result.expireInSeconds.toString(), res.result.userId.toString());
        }
      )
    );
  }

  getPerfil(id: number): Observable<User>{
    const params = new HttpParams()
    .set('Id', id.toString());
    const header = new HttpHeaders()
        .set('Authorization',  `Bearer ${this.getToken()}`);
    console.log(this.usuarioAuth);
    return this.httpClient.get<User>(this.usuarioAuth, {headers: header , params}   )
     .pipe(
       tap(
         (res: User) => {
           this.saveUser(res.result.roleNames, res.result.userName, res.result.emailAddress);
         }
       )
     );
  }

  validaToken(): Observable<any>{
    return this.httpClient.get(this.valida);
  }
  private saveUser(rol: string[], userName: string, email: string): void{
    this.cookies.set('rol', rol[0]);
    this.cookies.set('user', userName);
    this.cookies.set('email', email);
  }

  setToken(token: string, expiresIn: string, idToken: string): void {
    this.cookies.set('token', token);
    this.cookies.set('expiresIn', expiresIn);
    this.cookies.set('idToken', idToken);
  }
  getRol(): string {
    return this.cookies.get('rol');
  }
  getUser(): string {
    return this.cookies.get('user');
  }
  getToken(): string {
    return this.cookies.get('token');
  }

  getIdToken(): string {
    return this.cookies.get('idToken');
  }

  getEspiresIn(): string {
    return this.cookies.get('expiresIn');
  }
}
