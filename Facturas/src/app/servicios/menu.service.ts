import { ApiserviceService } from './apiservice.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  lista: any;
  perfil: string;
  constructor(private servicio: ApiserviceService) { }
  session = false;
  menuAdmin = [
    {name: 'Facturas', url: '/factura'},
    {name: 'Aprobaciones', url: '/aprobaciones'}
    ];
  menuCliente = [
    {name: 'Facturas', url: '/factura'}
  ];
  mostrarMenu(): void{

    console.log('ver menu');
    console.log(this.servicio.getToken());
    const id = this.servicio.getIdToken();
    console.log(id);
    const rol = this.servicio.getRol();
    console.log(rol);
    this.lista = null;
    this.perfil = rol;
    switch (rol){
      case 'ADMIN':
        this.lista = this.menuAdmin;
        break;
      case 'BACKOFFICE':
        this.lista = this.menuCliente;
        break;
    }
    this.session = true;
  }
}
