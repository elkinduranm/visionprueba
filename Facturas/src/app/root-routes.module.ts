import { AprobacionesComponent } from './contenido/aprobaciones/aprobaciones.component';
import { LoginComponent } from './contenido/login/login.component';
import { FacturaComponent } from './contenido/factura/factura.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [

  {path: 'factura', component: FacturaComponent},
  {path: 'aprobaciones', component: AprobacionesComponent},
  {path: '', component: LoginComponent}
];

export const app_routing = RouterModule.forRoot(routes);
